// CNewGraphicDlg.cpp : implementation file
//

#include "pch.h"
#include "Graphic.h"
#include "CNewGraphicDlg.h"
#include "afxdialogex.h"


// CNewGraphicDlg dialog

IMPLEMENT_DYNAMIC(CNewGraphicDlg, CDialog)

CNewGraphicDlg::CNewGraphicDlg(CArray<Graph*>* graphs, CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_CNewGraphicDlg, pParent), m_pConnection("ADODB.Connection"), m_pRecordset("ADODB.Recordset")
{
	m_pGraphs = graphs;

	CString conStr("");
	conStr += "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=";
	conStr += "Graphs1.accdb";
	conStr += ";";

	CoInitialize(NULL);


	try
	{
		m_pConnection->Open((_bstr_t)conStr, "", "", adConnectUnspecified);

		if (m_pRecordset->GetState()) m_pRecordset->Close();

		m_pRecordset->CursorLocation = adUseClient;

		m_pRecordset->Open((_bstr_t)"�������", (LPUNKNOWN)m_pConnection, adOpenDynamic, adLockOptimistic, adCmdTable);
	}
	catch (_com_error& error)
	{
		MessageBox(error.ErrorMessage());
	}
}
CNewGraphicDlg::~CNewGraphicDlg()
{
	if (m_pRecordset->GetState()) m_pRecordset->Close();
	m_pRecordset.Release();

	if (m_pConnection->GetState()) m_pConnection->Close();
	m_pConnection.Release();

	CoUninitialize();
}

CString CNewGraphicDlg::getFunc()
{
	CString str;
	m_function.GetWindowText(str);
	return str;
}

bool CNewGraphicDlg::isAdd()
{
	return m_add.GetCheck() == BST_CHECKED;
}

bool CNewGraphicDlg::isSave()
{
	return m_save.GetCheck() == BST_CHECKED;
}

float CNewGraphicDlg::getMin()
{
	CString str;

	m_min.GetWindowText(str);

	return std::stof((LPCSTR)str);
}

float CNewGraphicDlg::getMax()
{
	CString str;

	m_max.GetWindowText(str);

	return std::stof((LPCSTR)str);
}

void CNewGraphicDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_function);
	DDX_Control(pDX, IDC_CHECK1, m_add);
	DDX_Control(pDX, IDC_EDIT1, m_min);
	DDX_Control(pDX, IDC_EDIT3, m_max);
	DDX_Control(pDX, IDC_MFCCOLORBUTTON1, m_color);
	DDX_Control(pDX, IDC_CHECK2, m_save);

	while (!m_pRecordset->ADO_EOF)
	{
		m_function.AddString((_bstr_t)m_pRecordset->Fields->Item["���������"]->Value);
		m_pRecordset->MoveNext();
	}
	
}


BEGIN_MESSAGE_MAP(CNewGraphicDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CNewGraphicDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// CNewGraphicDlg message handlers

void CNewGraphicDlg::OnBnClickedOk()
{
	// TODO: �������� ���� ��� ����������� �����������
	try
	{
		Graph* graph = new Graph(
			getFunc(),
			getMin(),
			getMax(),
			m_color.GetColor()
		);

		if (isSave())
		{
			m_pRecordset->AddNew("���������", (_bstr_t)graph->getExpression());
		}

		if (!isAdd())
		{
			for (int i = 0; i < m_pGraphs->GetSize(); i++)
			{
				delete m_pGraphs->ElementAt(i);
			}
			m_pGraphs->RemoveAll();
		}
		
		m_pGraphs->Add(graph);

		CDialog::OnOK();
	}
	catch (const std::exception& e)
	{
		MessageBox(e.what());
	}
	
}


BOOL CNewGraphicDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	CRect rctComboBox, rctDropDown;
	//Combo rect
	m_function.GetClientRect(&rctComboBox);
	//DropDownList rect
	m_function.GetDroppedControlRect(&rctDropDown);

	//Get Item height
	int itemHeight = m_function.GetItemHeight(-1);
	//Converts coordinates
	m_function.GetParent()->ScreenToClient(&rctDropDown);
	//Set height
	rctDropDown.bottom = rctDropDown.top + rctComboBox.Height() + itemHeight * 3;
	//apply changes
	m_function.MoveWindow(&rctDropDown);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // ����������: �������� ������� OCX ������ ���������� �������� FALSE
}
