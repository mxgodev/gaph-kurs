#pragma once

// CNewGraphicDlg dialog
#include "Graph.h"

class CNewGraphicDlg : public CDialog
{
	DECLARE_DYNAMIC(CNewGraphicDlg)

public:
	CNewGraphicDlg(CArray<Graph*>* graphs, CWnd* pParent = nullptr);   // standard constructor

	virtual ~CNewGraphicDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CNewGraphicDlg };
#endif

protected:
	
	CString getFunc();
	bool isAdd();
	bool isSave();
	float getMin();
	float getMax();

	
	CArray<Graph*>* m_pGraphs;
	CArray<Graph*> m_pSavedGraphs;
	CComboBox m_function;
	CButton m_save;
	CButton m_add;
	CEdit m_min;
	CEdit m_max;
	CMFCColorButton m_color;

	_ConnectionPtr m_pConnection;
	_RecordsetPtr m_pRecordset;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	DECLARE_MESSAGE_MAP()
public:
	
	afx_msg void OnBnClickedOk();
	
	
	virtual BOOL OnInitDialog();
};
