﻿// CSettingsDlg.cpp: файл реализации
//

#include "pch.h"
#include "Graphic.h"
#include "CSettingsDlg.h"
#include "afxdialogex.h"


// Диалоговое окно CSettingsDlg

IMPLEMENT_DYNAMIC(CSettingsDlg, CDialog)

CSettingsDlg::CSettingsDlg(CWnd* pParent /*=nullptr*/)
	: CDialog(IDD_CSettingsDlg, pParent)
{

}

CSettingsDlg::~CSettingsDlg()
{
}

void CSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);

	Settings& settings = Settings::getInstance();
	CString t;


	DDX_Control(pDX, IDC_EDIT1, m_valOfDivision);
	DDX_Control(pDX, IDC_EDIT2, m_divisionSize);
	DDX_Control(pDX, IDC_EDIT3, m_drawStep);

	t.Format("%.2f", settings.getDivisionStep());
	m_valOfDivision.SetWindowText(t);

	t.Format("%d", settings.getValOfDivision());
	m_divisionSize.SetWindowText(t);

	t.Format("%.2f", settings.getDrawStep());
	m_drawStep.SetWindowText(t);
}

float CSettingsDlg::getValOfDivision()
{
	CString t;

	m_valOfDivision.GetWindowText(t);

	return std::stof((LPCSTR)t);
}

int CSettingsDlg::getDivisionSize()
{
	CString t;

	m_divisionSize.GetWindowText(t);

	return std::stoi((LPCSTR)t);
}

float CSettingsDlg::getDrawStep()
{
	CString t;

	m_drawStep.GetWindowText(t);

	return std::stof((LPCSTR)t);
}


BEGIN_MESSAGE_MAP(CSettingsDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CSettingsDlg::OnBnClickedOk)
END_MESSAGE_MAP()


// Обработчики сообщений CSettingsDlg


void CSettingsDlg::OnBnClickedOk()
{
	Settings& settings = Settings::getInstance();
	
	try
	{
		settings.setSettings(
			getDivisionSize(),
			getValOfDivision(),
			getDrawStep()
		);

		CDialog::OnOK();
	}
	catch (const std::exception& e)
	{
		MessageBox(e.what());
	}
}
