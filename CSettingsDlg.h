﻿#pragma once


// Диалоговое окно CSettingsDlg
#include "Settings.h"

class CSettingsDlg : public CDialog
{
	DECLARE_DYNAMIC(CSettingsDlg)

public:
	CSettingsDlg(CWnd* pParent = nullptr);   // стандартный конструктор
	virtual ~CSettingsDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CSettingsDlg };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

	float getValOfDivision();
	int getDivisionSize();
	float getDrawStep();

	DECLARE_MESSAGE_MAP()
public:
	CEdit m_valOfDivision;
	CEdit m_divisionSize;
	CEdit m_drawStep;
	afx_msg void OnBnClickedOk();
};
