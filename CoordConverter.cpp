#include "pch.h"
#include "CoordConverter.h"


CoordConverter::CoordConverter()
{
	updateValues();
}

void CoordConverter::updateValues()
{
	Settings& settings = Settings::getInstance();
	userToRealFactor = settings.getValOfDivision() / settings.getDivisionStep();
	realToUserFactor = settings.getDivisionStep() / settings.getValOfDivision() ;
}