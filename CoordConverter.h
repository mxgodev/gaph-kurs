#pragma once

#include "Singleton.h"
#include "Settings.h"

class CoordConverter : SettingsDependent
{
	DECLARE_SINGLETON_CLASS(CoordConverter)
public:
	int userToReal(float val, float scale) { return val / scale * userToRealFactor ; }
	double realToUser(int val, float scale) { return val / scale * realToUserFactor ; };
private:
	void updateValues() override;

	

private:
	float userToRealFactor;
	float realToUserFactor;
};

