#include "pch.h"
#include "Evaluator.h"

Evaluator& Evaluator::getInstance()
{
	static Evaluator instance;

	return instance;
}

float Evaluator::calc(float x)
{
	this->x = x;
	return expression.value();
}

bool Evaluator::setExpression(CString expression_string)
{
	return parser.compile((LPCSTR)expression_string, expression);
}

Evaluator::Evaluator()
{
	symbol_table.add_variable("x", x);
	symbol_table.add_constants();

	expression.register_symbol_table(symbol_table);
}
