#pragma once
class Evaluator
{
public:
	static Evaluator& getInstance();

	float calc(float x);
	bool setExpression(CString expression);

public:
	Evaluator(Evaluator&) = delete;
	void operator=(Evaluator&) = delete;

private:
	Evaluator();

	typedef exprtk::symbol_table<float> symbol_table_t;
	typedef exprtk::expression<float>     expression_t;
	typedef exprtk::parser<float>             parser_t;

	float x;
	symbol_table_t symbol_table;
	expression_t expression;
	parser_t parser;

};

