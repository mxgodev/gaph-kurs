#include "pch.h"
#include "Graph.h"

#include "Evaluator.h"
#include "Settings.h"

Graph::Graph()
{
	expression = "";
	min = max = 0;
	drawing = false;
	pen = nullptr;
	color = RGB(0,0,0);
}

Graph::Graph(CString expression, float min, float max, COLORREF color)
{
	if (!Evaluator::getInstance().setExpression(expression)) throw std::exception("������� ������� �����������");
	if (min == max) throw std::exception("������� ��������� �� ����� ���� �����");

	this->expression = expression;
	this->min = min;
	this->max = max;
	pen = new CPen(PS_SOLID, 1, color);
	this->color = color;
	drawing = false;	
}

Graph::~Graph()
{
	delete pen;
}

void Graph::draw(CDC* pDC, int cena_del, float step_del)
{
	Evaluator& ev = Evaluator::getInstance();
	CPen* pen_old = nullptr;

	float koef = cena_del / step_del;

	float step = Settings::getInstance().getDrawStep();

	if (!drawing) {
		ev.setExpression(expression);
		pen_old = pDC->SelectObject(pen);
		drawing = true;
	}

	pDC->MoveTo(min * koef, -ev.calc(min) * koef);

	float col_iter = abs(max - min) / step;
	int i = 0;
	float j = min + step;
	for (; i < col_iter; i++, j+=step)
	{
		//if (max - j < step) step = max - j;
		pDC->LineTo(j * koef, -ev.calc(j) * koef);
	}
	

	pDC->SelectObject(pen_old);
	drawing = false;
}

float Graph::getY(float x)
{
	Evaluator& ev = Evaluator::getInstance();

	ev.setExpression(expression);

	return ev.calc(x);
}
