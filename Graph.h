#pragma once
class Graph
{
public:
	Graph();
	Graph(CString expression, float min, float max, COLORREF color = RGB(0, 0, 0));
	~Graph();
	void draw(CDC* pDC, int cena_del, float step_del);

	CString& getExpression() { return expression; }
	float getMin() { return min; }
	float getMax() { return max; }
	CPen* getPen() { return pen; }
	COLORREF getColor(){ return color; }

	float getY(float x);
	
private:
	CString expression;
	float min, max;
	CPen* pen;
	COLORREF color;
	bool drawing;
};

