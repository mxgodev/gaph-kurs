﻿
// Graphic.h: основной файл заголовка для приложения Graphic
//
#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"       // основные символы


// CGraphicApp:
// Сведения о реализации этого класса: Graphic.cpp
//

class CGraphicApp : public CWinApp
{
public:
	CGraphicApp() noexcept;


// Переопределение
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Реализация
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CGraphicApp theApp;