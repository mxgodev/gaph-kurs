﻿// GraphicView.cpp: реализация класса CGraphicView
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS можно определить в обработчиках фильтров просмотра реализации проекта ATL, эскизов
// и поиска; позволяет совместно использовать код документа в данным проекте.
#ifndef SHARED_HANDLERS
#include "Graphic.h"
#endif

#include "GraphicDoc.h"
#include "GraphicView.h"

#include "Evaluator.h"
#include "CNewGraphicDlg.h"

#include "CoordConverter.h"

#include "CSettingsDlg.h"



#ifdef _DEBUG
#define new DEBUG_NEW  
#endif

// CGraphicView

IMPLEMENT_DYNCREATE(CGraphicView, CView)

BEGIN_MESSAGE_MAP(CGraphicView, CView)
	// Стандартные команды печати
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_MOUSEWHEEL()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
	ON_COMMAND(ID_NEW_GRAPHIC, &CGraphicView::OnNewGraphic)
	ON_WM_KEYDOWN()
	ON_WM_KEYUP()
	ON_WM_ERASEBKGND()
	ON_WM_NCPAINT()
	ON_COMMAND(ID_SETTINGS, &CGraphicView::OnSettings)
END_MESSAGE_MAP()

// Создание или уничтожение CGraphicView

CGraphicView::CGraphicView() noexcept 
{
	scale = 1;
	offsetX = offsetY = 0;
	startPoint = { 0,0 };
	mouse = { 0, 0 };

	
	

	//m_graphs.Add(new Graph("cos(x)", 0, 10, RGB(255, 0, 0)));
	//m_graphs.Add(new Graph("x*x", -3, 3));

	showValues = false;
}

CGraphicView::~CGraphicView()
{
	for (int i = 0; i < m_graphs.GetSize(); i++)
	{
		delete m_graphs[i];
	}
}

BOOL CGraphicView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: изменить класс Window или стили посредством изменения
	//  CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// Рисование CGraphicView


void CGraphicView::drawAxes(CDC* pDC)
{
	CString str;

	pDC->MoveTo(-(clientRect.right / 2 + offsetX), 0);
	pDC->LineTo(clientRect.right / 2 - offsetX, 0);

	pDC->MoveTo(0, -(clientRect.bottom / 2 + offsetY));
	pDC->LineTo(0, clientRect.bottom / 2 - offsetY);

	int step_devider = 1 / scale;
	

	int t = -clientRect.right / 2.0 - offsetX;

	for (float i = t - t % cena_del; i <= clientRect.right / 2.0 - offsetX; i += cena_del)
	{
		if (i == 0) continue;

		if (scale < 1.0)
		{
			if ((int)(i / cena_del) % step_devider != 0) continue;
		}

		pDC->MoveTo(i, -5);
		pDC->LineTo(i, 5);
		str.Format("%.2f", i / cena_del * step_del);

		pDC->TextOut(
			i - pDC->GetTextExtent(str).cx / 2.0,
			10,
			str
		);
	}

	t = -clientRect.bottom / 2.0 - offsetY;
	for (float i = t - t % cena_del; i <= clientRect.bottom / 2.0 - offsetY; i += cena_del)
	{
		if (i == 0) continue;

		if (scale < 1.0)
		{
			if ((int)(i / cena_del) % step_devider != 0) continue;
		}

		pDC->MoveTo(-5, i);
		pDC->LineTo(5, i);

		str.Format("%.2f", -(i / cena_del * step_del));

		pDC->TextOut(
			-pDC->GetTextExtent(str).cx - 10,
			i - pDC->GetTextExtent(str).cy / 2.0,
			str
		);
	}

	pDC->MoveTo(offsetLimit - 5, 5);
	pDC->LineTo(offsetLimit, 0);
	pDC->LineTo(offsetLimit - 5, -5);


	pDC->MoveTo(-5, -(offsetLimit - 5));
	pDC->LineTo(0, -(offsetLimit));
	pDC->LineTo(5, -(offsetLimit - 5));


	pDC->TextOut(-pDC->GetTextExtent("0").cx - 10, 10, "0");

	str = "X";
	pDC->TextOut(
		clientRect.right / 2.0 - pDC->GetTextExtent(str).cy - offsetX,
		pDC->GetTextExtent(str).cy,
		str
	);

	str = "Y";
	pDC->TextOut(
		-pDC->GetTextExtent(str).cy,
		-clientRect.bottom / 2.0 - offsetY,
		str
	);
}

void CGraphicView::drawGraphics(CDC* pDC)
{

	for (int i = 0; i < m_graphs.GetSize(); i++)
	{
		m_graphs[i]->draw(pDC, cena_del, step_del);
	}
}

void CGraphicView::drawGraphicsValues(CDC* pDC)
{
	Graph* graph;
	CPen* pen_old;
	CPen* pen;
	CString val;

	int x = mouse.x, y;

	float xUser;

	for (int i = 0; i < m_graphs.GetSize(); i++)
	{
		graph = m_graphs[i];

		if (x < graph->getMin() * cena_del / step_del) x = graph->getMin() * cena_del / step_del;
		if (x > graph->getMax() * cena_del / step_del) x = graph->getMax() * cena_del / step_del;
		
		xUser = x * step_del / cena_del;

		int y = -graph->getY(xUser) * cena_del / step_del;

		pen_old = pDC->SelectObject(graph->getPen());

		pDC->Ellipse(x - 5, y - 5, x + 5, y + 5);

		COLORREF color_old = pDC->SetTextColor(graph->getColor());

		val.Format("x: %.2f", xUser);
		pDC->TextOut(x + 10, y, val);
		val.Format("y :%.2f", graph->getY(xUser));
		pDC->TextOut(x + 10, y + pDC->GetTextExtent(val).cy, val);

		pDC->SelectObject(pen_old);
		pDC->SetTextColor(color_old);
	}
}

void CGraphicView::updateOffset()
{
	offsetLimit = 100 * cena_del / step_del;

	if (offsetX > offsetLimit - clientRect.right / 2) offsetX = offsetLimit - clientRect.right / 2;
	if (offsetX < -offsetLimit + clientRect.right / 2) offsetX = -offsetLimit + clientRect.right / 2;

	if (offsetY > offsetLimit - clientRect.bottom / 2) offsetY = offsetLimit - clientRect.bottom / 2;
	if (offsetY < -offsetLimit + clientRect.bottom / 2) offsetY = -offsetLimit + clientRect.bottom / 2;

}

void CGraphicView::OnDraw(CDC* pDC)
{
	CGraphicDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	Settings& settings = Settings::getInstance();



	CDC memDC;
	CBitmap bitmap, * bitmap_old;

	memDC.CreateCompatibleDC(pDC);
	bitmap.CreateCompatibleBitmap(pDC, clientRect.Width(), clientRect.Height());
	bitmap_old = memDC.SelectObject(&bitmap);

	memDC.SetViewportOrg(clientRect.right / 2 + offsetX, clientRect.bottom / 2 + offsetY);

	cena_del = settings.getValOfDivision() * scale;
	step_del = settings.getDivisionStep();

	offsetLimit = 100 * cena_del / step_del;

	GetClientRect(&clientRect);

	memDC.FillSolidRect(
		-(clientRect.right / 2 + offsetX),
		-(clientRect.bottom / 2 + offsetY),
		clientRect.right,
		clientRect.bottom,
		RGB(255,255,255)
	);

	

	drawAxes(&memDC);
	drawGraphics(&memDC);

	if (showValues)
	{
		drawGraphicsValues(&memDC);
	}

	pDC->BitBlt(
		clientRect.left, 
		clientRect.top, 
		clientRect.right, 
		clientRect.bottom, 
		&memDC, 
		-(clientRect.right / 2 + offsetX),
		-(clientRect.bottom / 2 + offsetY),
		SRCCOPY
	);
	memDC.SelectObject(bitmap_old);
}

// Печать CGraphicView

BOOL CGraphicView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// подготовка по умолчанию
	return DoPreparePrinting(pInfo);
}

void CGraphicView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: добавьте дополнительную инициализацию перед печатью
}

void CGraphicView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: добавьте очистку после печати
}

// Диагностика CGraphicView

#ifdef _DEBUG
void CGraphicView::AssertValid() const
{
	CView::AssertValid();
}

void CGraphicView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGraphicDoc* CGraphicView::GetDocument() const // встроена неотлаженная версия
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGraphicDoc)));
	return (CGraphicDoc*)m_pDocument;
}
#endif //_DEBUG


// Обработчики сообщений CGraphicView

BOOL CGraphicView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	int wheel = 0;

	float zoom;

	wheel = zDelta / 120;

	zoom = exp(wheel * 0.1);

	CString s;
	
	scale *= zoom;
	
	if (scale < 0.01) scale /= zoom;
	//MessageBox(t);



	Invalidate();
	

	s.Format("%f", scale);

	::AfxGetMainWnd()->SetWindowText(s);
	return CView::OnMouseWheel(nFlags, zDelta, pt);
}

void CGraphicView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	mouse.x = point.x - clientRect.right / 2 - offsetX;
	mouse.y = point.y - clientRect.bottom / 2 - offsetY;

	if (nFlags & MK_CONTROL)
	{
		
		CString t;
		t.Format("%d", point.x);
		::AfxGetMainWnd()->SetWindowText(t);
		Invalidate();
	}

	if (nFlags & MK_LBUTTON)
	{
		offsetX += point.x - startPoint.x;
		offsetY += point.y - startPoint.y;

		//updateOffset();

		CString t;
		t.Format("%d %d", offsetX, offsetY);
		::AfxGetMainWnd()->SetWindowText(t);


		startPoint = point;
		Invalidate();
	}

	CView::OnMouseMove(nFlags, point);
}

void CGraphicView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	startPoint = point;

	CView::OnRButtonDown(nFlags, point);
}


void CGraphicView::OnNewGraphic()
{
	CNewGraphicDlg dlg(&m_graphs);

	if (dlg.DoModal() == IDOK)
	{
		Invalidate();
	}
}


void CGraphicView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	/*CString t;

	t.Format("%d", nFlags);

	::AfxGetMainWnd()->SetWindowText(t);
*/


	if (nChar == VK_SPACE)
	{
		offsetX = 0;
		offsetY = 0;
		Invalidate();
	}

	if (nChar == VK_CONTROL && nFlags == 29)
	{
		showValues = true;
	
		Invalidate();
	}

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}


void CGraphicView::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	if (nChar == VK_CONTROL)
	{
		showValues = false;
		Invalidate();
	}
	CView::OnKeyUp(nChar, nRepCnt, nFlags);
}


BOOL CGraphicView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного

	Invalidate(false);
	return TRUE;
}


void CGraphicView::OnNcPaint()
{
	// TODO: добавьте свой код обработчика сообщений
	// Не вызывать CView::OnNcPaint() для сообщений рисования
}


void CGraphicView::OnSettings()
{
	CSettingsDlg dlg;

	if (dlg.DoModal() == IDOK)
	{
		Invalidate();
	}
}
