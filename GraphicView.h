﻿// GraphicView.h: интерфейс класса CGraphicView
//
#pragma once

#include "Graph.h"

//real = uesr * cena_del / step_del
//user = real * step_del / cena_del

class CGraphicView : public CView
{
protected: // создать только из сериализации
	CGraphicView() noexcept;
	DECLARE_DYNCREATE(CGraphicView)

// Атрибуты
public:
	CGraphicDoc* GetDocument() const;

	int offsetX, offsetY;

	int cena_del;
	float step_del;

	float realToUser;
	float userToReal;

	float scale;

	CPoint startPoint;
	CRect clientRect;

	CArray<Graph*> m_graphs;

	CPoint mouse;

	int offsetLimit;

	bool showValues;

// Операции
public:

	void drawAxes(CDC* pDC);
	void drawGraphics(CDC* pDC);
	void drawGraphicsValues(CDC* pDC);

	void updateOffset();

// Переопределение
public:
	virtual void OnDraw(CDC* pDC);  // переопределено для отрисовки этого представления
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
// Реализация
public:
	virtual ~CGraphicView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Созданные функции схемы сообщений
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnNewGraphic();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnNcPaint();
	afx_msg void OnSettings();
};

#ifndef _DEBUG  // версия отладки в GraphicView.cpp
inline CGraphicDoc* CGraphicView::GetDocument() const
   { return reinterpret_cast<CGraphicDoc*>(m_pDocument); }
#endif
