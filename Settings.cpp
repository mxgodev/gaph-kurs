#include "pch.h"
#include "Settings.h"

Settings::Settings()
{
	valOfDivision = 100;
	divisionStep = 1;
	drawStep = 0.05;
}

void Settings::setSettings(
	int valOfDivision,
	float divisionStep,
	float drawStep
)
{
	this->valOfDivision = valOfDivision;
	this->divisionStep = divisionStep;
	this->drawStep = drawStep;
}


SettingsDependent::SettingsDependent()
{
	Settings::getInstance().addDependency(this);
}

void Settings::addDependency(SettingsDependent* dependency)
{
	dependencies.Add(dependency);
}

void Settings::UpdateDependencies()
{
	for (int i = 0; i < dependencies.GetSize(); i++)
	{
		dependencies[i]->updateValues();
	}
}
