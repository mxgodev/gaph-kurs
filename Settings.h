#pragma once
#include "Singleton.h"

class Settings;

class SettingsDependent
{
	friend class Settings;
protected:
	SettingsDependent();
	virtual void updateValues() = 0;
};

class Settings
{
	DECLARE_SINGLETON_CLASS(Settings)
public:
	void addDependency(SettingsDependent*);

	int getValOfDivision() { return valOfDivision; }
	float getDivisionStep() { return divisionStep; }
	float getDrawStep() { return drawStep; }

	void setValOfDivision(int val) { valOfDivision = val; };
	void setDevesionStep(float val) { divisionStep = val; };
	void setDrawStep(float val) { drawStep = val; };

	void setSettings(
		int valOfDivision,
		float divisionStep,
		float drawStep
	);


private:
	CArray<SettingsDependent*> dependencies;
	void UpdateDependencies();

private:
	int valOfDivision;
	float divisionStep;
	float drawStep;
};

