#pragma once

#define DECLARE_SINGLETON_CLASS(TYPE) \
public: \
	TYPE(const TYPE&) = delete; \
	TYPE& operator=(const TYPE&) = delete; \
	static TYPE& getInstance() { \
		static TYPE instance; \
		return instance; \
	} \
protected: \
	TYPE(); \
	~TYPE() = default;