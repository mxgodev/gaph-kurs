﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется Graphic.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDR_MAINFRAME                   128
#define IDR_GraphicTYPE                 130
#define IDD_CNewGraphicDlg              310
#define IDD_CSettingsDlg                312
#define IDC_EDIT1                       1000
#define IDC_EDIT3                       1002
#define IDC_CHECK1                      1003
#define IDC_MFCCOLORBUTTON1             1004
#define IDC_CHECK2                      1005
#define IDC_COMBO1                      1006
#define IDC_EDIT2                       1007
#define ID_32771                        32771
#define ID_32772                        32772
#define ID_NEW_GRAPHIC                  32773
#define ID_SETTINGS                     32774

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        313
#define _APS_NEXT_COMMAND_VALUE         32775
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           313
#endif
#endif
